<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="../bootstrap/bootstrap.css">
<link rel="stylesheet" href="../bootstrap/mycss.css"> 
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dogs</title>
</head>
<body class="container">
<div>
<h1>Welcome to Dogs</h1> 
<ul class="nav nav-tabs">
  <li role="presentation"><a href="/capstone-review/app/">Home</a></li>
  <li role="presentation" class="active"><a href="dog">Dog</a></li>
  <li role="presentation"><a href="cat">Cat</a></li>
  <li role="presentation"><a href="bird">Burrrrd</a></li> 
</ul>
<img alt="puppies" src="../images/puppies.jpg" width="800" align="middle">
</div>
</body>
</html>