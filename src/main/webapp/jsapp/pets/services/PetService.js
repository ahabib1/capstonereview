mainApp.factory('PetService', function($resource) {
	
	var petsRes = $resource('/capstone-review/app/dogs');
	
	return {
		getPets: getPets
	};
	
	function getPets() {
		return petsRes.query();
	}
	
	
});