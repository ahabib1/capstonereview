package com.metlife.dpa.capstonereview.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CatController {
	
	@RequestMapping("/cat")
	public String index(){
		return "cat";
	}
	
	@RequestMapping(value="/cat", method=RequestMethod.GET)
    public String dogForm(Model model) {
        //model.addAttribute("formCat", new Cat());
        return "cat";
    }

}
