'use strict';

// Declare app level module which depends on views, and components
var mainApp = angular.module('mainApp', ['ngRoute', 'ngResource']);


mainApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/',
			{
				templateUrl: '/capstone-review/jsapp/pets/templates/home.tpl.html',
				controller: 'homeCtrl',
				resolve: {
					pets:function(PetService){
						return PetService.getPets();
					}
				}
			}
	);
	
	$routeProvider.when('/dog',
			{
				templateUrl: '/capstone-review/jsapp/pets/templates/dog.tpl.html',
				controller: 'dogCtrl',
				resolve: {
					pets:function(PetService){
						return PetService.getPets();
					}
				}
			}
	);
	
	$routeProvider.when('/cat',
			{
				templateUrl: '/capstone-review/jsapp/pets/templates/cat.tpl.html',
				controller: 'catCtrl',
				resolve: {
					pets:function(PetService){
						return PetService.getPets();
					}
				}
			}
	);
	
	$routeProvider.when('/bird',
			{
				templateUrl: '/capstone-review/jsapp/pets/templates/bird.tpl.html',
				controller: 'birdCtrl',
				resolve: {
					pets:function(PetService){
						return PetService.getPets();
					}
				}
			}
	);
	
	$routeProvider.otherwise({redirectTo: 'main'});
}]);