<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="../bootstrap/bootstrap.css">
<link rel="stylesheet" href="../bootstrap/mycss.css"> 
<title>Petopia</title>
</head>
<body class="container">
<div>
<h1>Welcome to Petopia <img alt="Neopets" src="../images/neopets.png" /></h1>
<ul class="nav nav-pills">
  <li role="presentation"><a href="/capstone-review/app/">Home</a></li>
  <li role="presentation"><a href="#/dog">Dog</a></li>
  <li role="presentation"><a href="#/cat">Cat</a></li>
  <li role="presentation"><a href="#/bird">Burrrrd</a></li> 
</ul>

</div>
<body ng-app="mainApp">
<div ng-view></div>

  <script src="/capstone-review/jsapp/bower_components/jquery/jquery.js"></script>
  <script src="/capstone-review/jsapp/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>  
  <script src="/capstone-review/jsapp/bower_components/angular/angular.js"></script>
  <script src="/capstone-review/jsapp/bower_components/angular-route/angular-route.min.js"></script>
  <script src="/capstone-review/jsapp/bower_components/angular-resource/angular-resource.min.js"></script>
  <script src="/capstone-review/jsapp/pets/app.js"></script>
  <script src="/capstone-review/jsapp/pets/controllers/dogCtrl.js"></script>
  <script src="/capstone-review/jsapp/pets/controllers/catCtrl.js"></script>
  <script src="/capstone-review/jsapp/pets/controllers/birdCtrl.js"></script>
  <script src="/capstone-review/jsapp/pets/controllers/homeCtrl.js"></script>
  <script src="/capstone-review/jsapp/pets/services/PetService.js"></script>
	
</body>
</html>