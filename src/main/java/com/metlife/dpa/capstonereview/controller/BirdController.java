package com.metlife.dpa.capstonereview.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class BirdController {
	
	@RequestMapping("/bird")
	public String index(){
		return "bird";
	}
	
	@RequestMapping(value="/bird", method=RequestMethod.GET)
    public String dogForm(Model model) {
        //model.addAttribute("formDog", new Dog());
        return "bird";
    }

}

