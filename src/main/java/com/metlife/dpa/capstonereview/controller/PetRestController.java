package com.metlife.dpa.capstonereview.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metlife.dpa.capstonereview.model.Pet;
import com.metlife.dpa.capstonereview.model.Player;
import com.metlife.dpa.capstonereview.repository.PetRepository;

@RestController
public class PetRestController {

	@Autowired
	PetRepository petRepository;
	
	@RequestMapping("/dogs")
	public List<Pet> pets(){
		return petRepository.findAll();
	}
	
	@RequestMapping(value="/seedpets", method=RequestMethod.POST)
	public void seedPets(){
		List<Pet> pets = new ArrayList<Pet>();
		pets.add(new Pet("Fido", "Dog", "Lab", "Female", "2"));
		pets.add(new Pet("Katniss", "Cat", "Persian", "Female", "4"));
		pets.add(new Pet("Tweety", "Bird", "Parakeet", "Male", "3"));
		
		petRepository.save(pets);
	}
}
